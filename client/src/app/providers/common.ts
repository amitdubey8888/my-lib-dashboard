import {Injectable} from '@angular/core';
import * as _ from 'lodash';

@Injectable()
export class Common {

  constructor() {
  }

  checkSupportedFormat(file, type) {
    if (file.type.split('/')[0] == type) {
      if (file.type.split('/')[0] === "video") {
        if (this.supportedVideoFormats.findIndex(format => format == file.type.split('/')[1]) != -1)
          return true
      }
      else if (file.type.split('/')[0] === "image") {
        if (this.supportedImageFormats.findIndex(format => format == file.type.split('/')[1]) != -1)
          return true
      }
      else {
        return false
      }
    }
    else
      return false
  }

  supportedImageFormats = [
    'jpeg',
    'jpg',
    'gif',
    'png',
    'bmp',
    'webp'
  ];
  supportedVideoFormats = [
    '3gp',
    'mp4',
    'ts',
    'webm',
    'mkv'
  ]
}
