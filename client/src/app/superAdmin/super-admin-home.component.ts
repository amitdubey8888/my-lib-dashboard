import {Component, EventEmitter} from "@angular/core";
import {MaterializeAction} from "angular2-materialize/dist/index";
import {CurrentUser} from '../providers/currentUser';
import {NgForm} from "@angular/forms";
import {Api} from '../providers/api';
import {ActivatedRoute, Router} from "@angular/router";
import {LoginService} from '../providers/login.service';
import * as Materialize from "angular2-materialize/dist/index";


@Component({
  selector: 'super-admin-home',
  templateUrl: './super-admin-home.component.html',
  styleUrls: ['./super-admin-home.component.css']
})
export class SuperAdminHomeComponent {
  private admins = [];

  newAdmin = {
    username: '',
    phone: '',
    email: '',
    password: '',
    realm: 'admin',
  };
  editAdmin = {
    username: '',
    phone: '',
    email: '',
    password: '',
    realm: 'admin',
    userId: ''
  };
  password2 = '';
  loading = false;

  constructor(private api:Api,
              public loginService:LoginService,
              public router:Router,
              public route:ActivatedRoute) {
    this.getAdmins();
  }

  createAdmin(form:NgForm) {
    this.loading = true;
    this.api.post('users', this.newAdmin)
      .map(res => res.json())
      .subscribe(
        data => {
          this.loading = false;
          Materialize.toast(`Created new admin ${this.newAdmin.username}`, 5000);
          form.resetForm();
          this.closeModal();
          this.getAdmins();
        },
        error => {
          this.loading = false;
          Materialize.toast(`${JSON.parse(error._body).error.message}`, 5000);
        })
  }

  getAdmins() {
    this.loading = true;
    this.api.get(`users?filter[where][realm]=admin`)
      .map(res => res.json())
      .subscribe(
        res => {
          this.admins = res;
          this.loading = false;
        },
        error => {
          // console.error(error);
          this.loading = false;
          Materialize.toast(`Unable to connect, Please check your connection`, Infinity)
        }
      )
  }

  updateAdmin(form:NgForm):void {
    this.loading = true;
    let details = Object.assign({}, this.editAdmin);
    this.api.put("users/updateUser", {details: details})
      .map(res => res.json())
      .subscribe(
        response => {
          this.loading = false;
          // console.info("response", response);
          Materialize.toast(`Successfully updated user ${this.editAdmin.username}`, 5000);
          this.getAdmins();
          // form.resetForm();
          this.closeModalEdit();
        },
        error => {
          this.loading = false;
          Materialize.toast(`Oops! Something bad happened, retry`, 5000);
          // console.log(error)
        }
      );
  }

  deleteAdmin(admin):void {
    this.loading = true;
    this.api.delete('users/' + admin.userId)
      .map(res => res.json())
      .subscribe((response) => {
          this.loading = false;
          Materialize.toast(`Successfully deleted ${admin.username}`, 5000);
          // console.log("delete response", response);
          this.getAdmins();
        },
        error => {
          this.loading = false;
          Materialize.toast(`Oops! Something bad happened, retry`, 5000);
          // console.log(error)
        });
  }

  modalActionsAdd = new EventEmitter<string|MaterializeAction>();
  modalActionsEdit = new EventEmitter<string|MaterializeAction>();

  openModal() {
    this.modalActionsAdd.emit({action: "modal", params: ['open']});
  }

  closeModal() {
    this.modalActionsAdd.emit({action: "modal", params: ['close']});
  }

  openModalEdit(editAdmin) {
    this.editAdmin = Object.assign({}, editAdmin);
    this.modalActionsEdit.emit({action: "modal", params: ['open']});
  }

  closeModalEdit() {
    this.modalActionsEdit.emit({action: "modal", params: ['close']});
  }
}
