import {Component, OnInit} from "@angular/core";
import {NgForm} from "@angular/forms";
import {Router, ActivatedRoute} from '@angular/router';
import {LoginService} from "../providers/login.service"
import * as Materialize from "angular2-materialize/dist/index";
import {CurrentUser} from "../providers/currentUser";

@Component({
  selector: 'super-admin-login',
  providers: [LoginService],
  templateUrl: "./super-admin-login.component.html",
  styleUrls: ['./super-admin-login.component.css']
})

export class SuperAdminLoginComponent implements OnInit {

  username = 'demonVibe';
  password = 'demonVibe';
  loading = false;
  returnUrl:string;

  constructor(private loginService:LoginService,
              private route:ActivatedRoute,
              private currentUser:CurrentUser,
              private router:Router) {
  }

  ngOnInit() {
    this.returnUrl = this.route.snapshot.queryParams['returnUrl'] || '/super/home';
    if (!this.currentUser.info) {
      let userToken = localStorage.getItem('superUserToken') || null;
      userToken = JSON.parse(userToken);
      if (userToken) {
        this.loading = true;
        this.loginService.getDetailsViaToken(userToken)
          .subscribe(
            user => {
              this.loading = false;
              this.currentUser.info = user.json();
              this.router.navigate([this.returnUrl]);
            },
            error => {
              this.loading = false;
              Materialize.toast(`Unable to connect, Please check your connection`, Infinity)
            }
          );
      }
      else {
        this.currentUser.info = null;
      }
    }
    else if (this.currentUser.info.realm == 'superuser') {
      this.router.navigate([this.returnUrl]);
    }
    else {
      this.currentUser.info = null;
    }
  }

  onSubmit(form:NgForm) {
    this.loading = true;
    this.loginService.customLogin(this.username, this.password, 'superuser')
      .subscribe(
        data => {
          console.log("data", data);
          this.loading = false;
          Materialize.toast(`Logged in as ${this.username}`, 5000);
          form.resetForm();
          this.router.navigate(['/super/home']);
        },
        error => {
          this.loading = false;
          if (error.status == 0)
            Materialize.toast(`Oops! Something went bad, Please try after some time.`, 5000);
          else {
            Materialize.toast(`Username or password incorrect!`, 5000);
          }
        });
  }
}
