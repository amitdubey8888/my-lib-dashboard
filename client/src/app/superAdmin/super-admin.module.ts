import {NgModule} from "@angular/core";
import {CommonModule} from "@angular/common";
import {FormsModule} from "@angular/forms";
import {SuperAdminMenuComponent} from "./super-admin-menu.component";
import {SuperAdminRouting} from "./super-admin-rounting.module";
import {SuperAdminHomeComponent} from "./super-admin-home.component";
import {SuperAdminSettingComponent} from "./super-admin-setting.component";
import {MaterializeModule} from "angular2-materialize/dist/index";
import {SuperAdminLoginComponent} from "./super-admin-login.component";
@NgModule({
  imports:[
    CommonModule,
    FormsModule,
    SuperAdminRouting,
    MaterializeModule
  ],
  declarations:[
    SuperAdminMenuComponent,
    SuperAdminHomeComponent,
    SuperAdminSettingComponent,
    SuperAdminLoginComponent
  ]
})
export class SuperAdminModule{}
