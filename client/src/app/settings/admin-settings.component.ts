import {Component, OnInit} from "@angular/core";
import {AngularFire, FirebaseListObservable} from 'angularfire2';
import * as firebase from 'firebase';
import {Api} from '../providers/api';
import {CurrentUser} from '../providers/currentUser';
import {LoginService} from '../providers/login.service'
import {ActivatedRoute, Router} from "@angular/router";
import {Common} from "../providers/common";
import * as Materialize from "angular2-materialize/dist/index";

@Component({
  selector: 'admin-settings',
  styleUrls: ['./admin-settings.component.css'],
  templateUrl: "./admin-settings.component.html"
})
export class AdminSettingsComponent implements OnInit {

  public storageRef = firebase.storage().ref();
  public loading:boolean = false;
  public preferences = {
    "background": "",
    "privacy_policy": "",
    "terms": "",
    "licences": "",
    "help": ""
  };
  public preferenceId = "";
  public currentEdit:any = {};
  public obj:any = {};
  public settings = [
    {display: 'Help & Feedback', icon: 'help_outline', id: 'help'},
    {display: 'License', icon: 'copyright', id: 'licences'},
    {display: 'Terms and Conditions', icon: 'assignment', id: 'terms'},
    {display: 'Privacy & Policy', icon: 'lock_outline', id: 'privacy_policy'}
  ];
  private editor = false;

  constructor(public af:AngularFire,
              public api:Api,
              public currentUser:CurrentUser,
              public common:Common,
              public loginService:LoginService,
              public router:Router,
              public route:ActivatedRoute) {
  }

  ngOnInit() {
    this.loading = true;
    if (!this.currentUser.info) {
      let userToken = localStorage.getItem('currentUserToken') || null;
      userToken = JSON.parse(userToken);
      if (userToken) {
        this.loginService.getDetailsViaToken(userToken)
          .map(res => res.json())
          .subscribe(
            res => {
              this.loading = false;
              this.currentUser.info = res;
            },
            error => {
              this.loading = false;
            });
      } else {
        this.loading = false;
        this.router.navigate(['../../login'], {relativeTo: this.route});
        Materialize.toast(`You need to be logged in to continue...`, 5000);
      }
    } else {
      this.loading = false;
      // console.info("info", this.currentUser.info);
    }
    this.getPreferences();
  }

  showEditor(page) {
    this.currentEdit = page;
    this.editor = true;
  }

  hideEditor() {
    this.editor = false;
  }

  saveContent() {
    this.editor = false;
    this.savePreferences();
  }

  bgUpload(event) {
    this.loading = true;
    let file = event.srcElement.files[0];
    if (file && this.common.checkSupportedFormat(file, 'image')) {
      this.storageRef.child(`${'settings/images/background'}/${new Date().getTime() + file.name}`).put(file)
      .then(snapshot => {
        this.loading = false;
        this.preferences.background = snapshot.downloadURL;
        this.saveBgImage();
      })
      .catch(error => {
        this.loading = false;
        Materialize.toast(`Oops! Something bad happened, retry`, 5000);
      })
    }
    else {
      Materialize.toast(`Unsupported file or not an image`, 5000);
      this.loading = false;
    }
  }

  saveBgImage() {
    this.loading = true;
    this.api.patch('app_preferences/' + this.preferenceId, this.preferences)
      .map(res => res.json())
      .subscribe(
        response => {
          this.loading = false;
          Materialize.toast(`Image saved successfully`, 5000);
        },
        error => {
          this.loading = false;
          Materialize.toast(`Unable to save image, retry`, 5000);
        })
  }

  getPreferences() {
    this.loading = true;
    this.api.get(`app_preferences`)
      .map(res => res.json())
      .subscribe(
        res => {
          if (res.length) {
            this.loading = false;
            this.preferences = res[0];
            this.preferenceId = res[0].id;
          }
          else {
            this.loading = true;
            this.api.post(`app_preferences`, this.preferences)
              .map(res => res.json())
              .subscribe(
                res => {
                  this.loading = false;
                  this.preferences = res;
                  this.preferenceId = res.id;
                },
                error => {
                  this.loading = false;
                  Materialize.toast(`Unable to fetch preferences, retry`, 5000);
                  // console.error(error)
                });
          }
        },
        error => {
          this.loading = false;
          Materialize.toast(`Oops! Something bad happened, retry`, 5000);
        });
  }

  savePreferences() {
    let data:any = {};
    switch (this.currentEdit.id) {
      case 'help':
        data.help = this.preferences.help;
        break;
      case 'licences':
        data.licences = this.preferences.licences;
        break;
      case 'terms':
        data.terms = this.preferences.terms;
        break;
      case 'privacy_policy':
        data.privacy_policy = this.preferences.privacy_policy;
        break;
    }
    this.loading = true;
    this.api.patch('app_preferences/' + this.preferenceId, this.preferences)
      .map(res => res.json())
      .subscribe(
        response => {
          this.loading = false;
          Materialize.toast(`Updated successfully`, 5000);
          // console.log("Success", response)
        },
        error => {
          this.loading = false;
          Materialize.toast(`Oops! Something bad happened, retry`, 5000);
        })
  }
}
