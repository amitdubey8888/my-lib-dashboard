import {Component, EventEmitter} from "@angular/core";
@Component({
  selector: 'admin-campaign',
  templateUrl: "./admin-campaign.component.html"
})
export class AdminCampaignComponent{
private _messages = [{heading: "Message 1", content: "this is sample message"},
  {heading: "Message 2", content: "this is sample message"},
  {heading: "Message 3", content: "this is sample message"},
  {heading: "Message 4", content: "this is sample message"},
  {heading: "Message 5", content: "this is sample message"},
  {heading: "Message 6", content: "this is sample message"},]

  private _users = [{username:'ajay',id:1,selected:false},
    {username:'vjay',id:2,selected:false},
    {username:'rahul',id:3,selected:false},
    {username:'mahesh',id:4,selected:false},
    {username:'rohit',id:5,selected:false},
    {username:'anand',id:6,selected:false},
  ]

  private newMessage = false;
  addMessage(){
    this.newMessage = true;
  }
  showMessage(){
    this.newMessage = false;
  }
}
