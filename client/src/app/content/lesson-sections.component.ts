import {Subscription} from "rxjs/Subscription";
import {Component, OnDestroy, OnInit} from "@angular/core";
import {ActivatedRoute, Router} from "@angular/router";
import {Api} from '../providers/api';
import {AngularFire} from 'angularfire2';
import * as firebase from 'firebase';
import * as _ from 'lodash'
import * as Materialize from "angular2-materialize/dist/index";
import {Common} from '../providers/common'

@Component({
  selector: 'lesson-section',
  styleUrls: ["./lesson-sections.component.css"],
  templateUrl: "./lesson-sections.component.html",
})

export class LessonSectionsComponent implements OnDestroy, OnInit {
  public lessonId;
  public lesson_title;
  public lesson_icon;
  public lsections = [];
  public topicId:string;
  private loading:boolean = false;

  paramsSubscription:Subscription;
  public storageRef = firebase.storage().ref();

  constructor(public router:Router,
              public route:ActivatedRoute,
              public common:Common,
              public api:Api) {
  }

  ngOnDestroy() {
    this.paramsSubscription.unsubscribe();
  }

  ngOnInit() {
    this.paramsSubscription = this.route.params.subscribe(params => this.lessonId = params['lessonId']);
    console.info("Received lessonId ", this.lessonId);
    this.fetchLesson();
    this.getSections();
  }

  saveSections():void {
    this.api.patch(`lessons/${this.lessonId}`, {
      "lesson_title": this.lesson_title,
      "lesson_icon": this.lesson_icon,
      "topicId": this.topicId
    })
      .map(res => res.json())
      .subscribe(
        res => {
          Materialize.toast(`Lesson updated successfully`, 5000);
          _.forEach(this.lsections, lsection => {
            let lid = lsection.id;
            delete lsection.id;
            lsection.lessonId = res.id;
            if (lid) {
              this.api.patch(`lsections/${lid}`, lsection)
                .map(response => response.json())
                .subscribe(
                  response => {
                    // Materialize.toast(`Lesson updated successfully`, 5000);
                    this.router.navigate(['../../topics', this.topicId], {relativeTo: this.route});
                  }, error => {
                    console.error("error in saving lsection");
                    // Materialize.toast(`Oops! Something bad happened, retry`, 5000);
                  })
            }
            else {
              this.api.post(`lsections/`, lsection)
                .map(response => response.json())
                .subscribe(
                  response => {
                    // console.log("Success", response);
                    this.router.navigate(['../../topics', this.topicId], {relativeTo: this.route});
                  },
                  error => {
                    console.error("error in lsection saving");
                    //_todo Handle error in async
                  })
            }
          });
        },
        error => {
          Materialize.toast(`Oops! Something bad happened, retry`, 5000);
        });
  }

  addNewSection() {
    let idlength = this.lsections.length;
    this.lsections.push({
      "mediaURI": "",
      "imageURI": "",
      "section_description": ""
    })
  }

  fetchLesson() {
    this.loading = true;
    this.api.get(`lessons/${this.lessonId}`)
      .map(res => res.json())
      .subscribe(
        response => {
          this.loading = false;
          this.lesson_icon = response.lesson_icon;
          this.lesson_title = response.lesson_title;
          this.topicId = response.topicId
        },
        error => {
          this.loading = false;
          Materialize.toast(`Unable to fetch lesson`, 5000);
        }
      )
  }

  getSections() {
    this.loading = true;
    this.api.get(`lsections?filter={"where": {"lessonId": "${this.lessonId}"}}`)
      .map(res => res.json())
      .subscribe(
        response => {
          this.loading = false;
          this.lsections = response;
        },
        error => {
          this.loading = false;
          Materialize.toast(`Unable to fetch lesson details`, 5000);
        }
      );
  }

  lessonIconUpload(event) {
    this.loading = true;
    let file = event.srcElement.files[0];
    if (file && this.common.checkSupportedFormat(file, 'image')) {
      this.storageRef.child(`${'lessons/icons'}/${new Date().getTime() + file.name}`).put(file)
        .then(snapshot => {
          this.loading = false;
          this.lesson_icon = snapshot.downloadURL;
          Materialize.toast(`Lesson Icon successfully uploaded`, 5000);
        })
        .catch(error => {
          this.loading = false;
          Materialize.toast(`Oops! Something bad happened, retry`, 5000);
        })
    }
    else {
      Materialize.toast(`Unsupported file or not an image`, 5000);
      this.loading = false;
    }
  }

  uploadMedia(event, i) {
    this.loading = true;
    let file = event.srcElement.files[0];
    if (file && this.common.checkSupportedFormat(file, 'video')) {
      this.storageRef.child(`${'lessons/videos'}/${new Date().getTime() + file.name}`).put(file)
        .then(snapshot => {
          this.loading = false;
          this.lsections[i].mediaURI = snapshot.downloadURL;
          Materialize.toast(`Media successfully uploaded`, 5000);
        })
        .catch(error => {
          this.loading = false;
          Materialize.toast(`Oops! Something bad happened, retry`, 5000);
        })
    }
    else {
      Materialize.toast(`Unsupported file or not a video`, 5000);
      this.loading = false;
    }
  }

  attachImage(event, i) {
    this.loading = true;
    let file = event.srcElement.files[0];
    if (file && this.common.checkSupportedFormat(file, 'image')) {
      this.storageRef.child(`${'lessons/pictures'}/${new Date().getTime() + file.name}`).put(file)
        .then(snapshot => {
          this.loading = false;
          Materialize.toast(`Image successfully uploaded`, 5000);
          this.lsections[i].imageURI = snapshot.downloadURL;
        })
        .catch(error => {
          this.loading = false;
          Materialize.toast(`Oops! Something bad happened, retry`, 5000);
        })
    }
    else {
      Materialize.toast(`Unsupported file or not an image`, 5000);
      this.loading = false;
    }
  }
}
