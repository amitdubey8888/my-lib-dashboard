import {Subscription} from "rxjs/Subscription";
import {Component, EventEmitter, OnDestroy, OnInit} from "@angular/core";
import {MaterializeAction} from "angular2-materialize/dist/index";
import {ActivatedRoute, Router} from "@angular/router";
import {Api} from '../providers/api';
import {UploadImagesService} from '../providers/upload-images.service';
import {AngularFire, FirebaseListObservable} from 'angularfire2';
import * as firebase from 'firebase';
import * as Materialize from "angular2-materialize/dist/index";
import {Common} from "../providers/common";

@Component({
  selector: 'subject-books',
  styleUrls: ['./subject-books.component.css'],
  templateUrl: "./subject-books.component.html",
})

export class SubjectBooksComponent implements OnDestroy, OnInit {
  public subjectId;
  public books = [];
  public loading:boolean = false;
  public newBook = {
    "book_title": "",
    "book_description": "",
    "book_cover": "",
    "book_thumb": "",
    "book_bg": "",
    "termsCount": null,
    "subjectId": "",
    "termsIcon": ""
  };
  public editBook = {
    "book_title": "",
    "book_description": "",
    "book_cover": "",
    "book_thumb": "",
    "book_bg": "",
    "termsCount": null,
    "subjectId": "",
    "id": "",
    "termsIcon": ""
  };
  paramsSubscription:Subscription;

  public bookImages = {
    thumb: {},
    background: {}
  };
  public images = [];
  public isEnabledUpload = true;
  public storageRef = firebase.storage().ref();

  constructor(public router:Router,
              public route:ActivatedRoute,
              public api:Api,
              public common:Common,
              public upload:UploadImagesService,
              public af:AngularFire) {
  }

  ngOnInit() {
    this.paramsSubscription = this.route.params.subscribe(params => {
      this.subjectId = params['subjectId'];
    });
    this.getBooks();
  }

  ngOnDestroy() {
    this.paramsSubscription.unsubscribe();
  }

  onSelect(book) {
    this.router.navigate(['../../books', book.id], {relativeTo: this.route});
  }

  addBook(form):void {
    this.loading = true;
    this.newBook.subjectId = this.subjectId;
    this.api.post('books', this.newBook)
      .map(res => res.json())
      .subscribe(
        response => {
          this.getBooks();
          this.loading = false;
          Materialize.toast(`${this.newBook.book_title} added successfully`, 5000);
          this.closeModalAdd(form);
        },
        error => {
          this.loading = false;
          Materialize.toast(`Oops! Something bad happened, retry`, 5000);
        });
  }

  getBooks():void {
    this.loading = true;
    this.api.get('books?filter[where][subjectId]=' + this.subjectId)
      .map(res => res.json())
      .subscribe(
        response => {
          this.loading = false;
          this.books = response;
        },
        error => {
          this.loading = false;
          Materialize.toast(`Oops! Something bad happened, retry`, 5000);
        })
  }

  updateBook(form):void {
    this.loading = true;
    this.api.patch('books/' + this.editBook.id, this.editBook)
      .map(res => res.json())
      .subscribe(
        response => {
          this.loading = false;
          this.getBooks();
          Materialize.toast(`${this.editBook.book_title} updated successfully`, 5000);
          this.closeModalEdit(form);
        },
        error => {
          this.loading = false;
          Materialize.toast(`Oops! Something bad happened, retry`, 5000);
        })
  }

  deleteBook(book):void {
    this.loading = true;
    console.warn("sub", book.id);
    this.api.delete('books/' + book.id)
      .map(res => res.json())
      .subscribe(
        () => {
          this.loading = false;
          this.getBooks();
          Materialize.toast(`${book.book_title} deleted successfully`, 5000);
        },
        () => {
          this.loading = false;
          Materialize.toast(`Oops! Something bad happened, retry`, 5000);
        })
  }

  bookThumbUpload(event, edit) {
    this.loading = true;
    let that = this;
    let file = event.srcElement.files[0];
    if (file && this.common.checkSupportedFormat(file, 'image')) {
      this.storageRef.child(`${'books/images/thumb'}/${new Date().getTime() + file.name}`).put(file)
        .then(function (snapshot) {
          that.loading = false;
          // console.log('URL', snapshot.downloadURL);
          if (edit === false) {
            that.newBook.book_thumb = snapshot.downloadURL;
          } else {
            that.editBook.book_thumb = snapshot.downloadURL;
          }
        });
    }
    else {
      Materialize.toast(`Unsupported file or not an image`, 5000);
      this.loading = false;
    }
  }

  bookBgUpload(event, edit) {
    // console.log(edit);
    this.loading = true;
    let that = this;
    let file = event.srcElement.files[0];
    if (file && this.common.checkSupportedFormat(file, 'image')) {
      this.storageRef.child(`${'books/images/background'}/${new Date().getTime() + file.name}`).put(file)
        .then(function (snapshot) {
          that.loading = false;
          // console.log('URL', snapshot.downloadURL);
          if (edit === false) {
            that.newBook.book_bg = snapshot.downloadURL;
          } else {
            that.editBook.book_bg = snapshot.downloadURL;
          }
        })
    }
    else {
      Materialize.toast(`Unsupported file or not an image`, 5000);
      this.loading = false;
    }
  }

  termIconUpload(event, edit) {
    // console.log(edit);
    this.loading = true;
    let that = this;
    let file = event.srcElement.files[0];
    if (file && this.common.checkSupportedFormat(file, 'image')) {
      this.storageRef.child(`${'books/images/termsIcon'}/${new Date().getTime() + file.name}`).put(file)
        .then(function (snapshot) {
          // console.log('URL', snapshot.downloadURL);
          that.loading = false;
          if (edit === false) {
            that.newBook.termsIcon = snapshot.downloadURL;
          } else {
            that.editBook.termsIcon = snapshot.downloadURL;
          }
        })
    }
    else {
      Materialize.toast(`Unsupported file or not an image`, 5000);
      this.loading = false;
    }
  }

  modalActionsAdd = new EventEmitter<string | MaterializeAction>();
  modalActionsEdit = new EventEmitter<string | MaterializeAction>();

  openModalAdd() {
    this.modalActionsAdd.emit({action: "modal", params: ['open']});
  }

  closeModalAdd(form) {
    form.resetForm();
    this.modalActionsAdd.emit({action: "modal", params: ['close']});
  }

  openModalEdit(book) {
    this.editBook = Object.assign({}, book);
    this.modalActionsEdit.emit({action: "modal", params: ['open']});
  }

  closeModalEdit(form) {
    form.resetForm();
    this.modalActionsEdit.emit({action: "modal", params: ['close']});
  }
}
