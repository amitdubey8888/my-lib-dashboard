import {Subscription} from "rxjs/Subscription";
import {Component, OnDestroy, OnInit} from "@angular/core";
import {ActivatedRoute, Router} from "@angular/router";
import {Api} from '../providers/api';
import {AngularFire} from 'angularfire2';
import * as firebase from 'firebase';
import * as _ from 'lodash'
import * as Materialize from "angular2-materialize/dist/index";
import {Common} from "../providers/common";

@Component({
  selector: 'add-section',
  styleUrls: ["./add-sections.component.css"],
  templateUrl: "./add-sections.component.html",
})

export class AddSectionsComponent implements OnDestroy, OnInit {
  public topicId;
  public lesson_title;
  public lesson_icon;
  public lsections = [];
  private loading:boolean = false;
  private videoUpdate:boolean = true;
  paramsSubscription:Subscription;
  public storageRef = firebase.storage().ref();

  constructor(public router:Router,
              public route:ActivatedRoute,
              public api:Api,
              public common:Common,
              public af:AngularFire) {
  }

  ngOnDestroy() {
    this.paramsSubscription.unsubscribe();
  }

  ngOnInit() {
    this.paramsSubscription = this.route.params.subscribe(params => this.topicId = params['topicId']);
    this.addNewSection();
  }

  saveSections() {
    this.loading = true;
    this.api.post("lessons", {
      "lesson_title": this.lesson_title,
      "lesson_icon": this.lesson_icon,
      "topicId": this.topicId
    })
      .map(res => res.json())
      .subscribe(
        res => {
          console.log("res", res);
          _.forEach(this.lsections, lsection => lsection.lessonId = res.id);
          this.api.post("lsections", this.lsections)
            .map(response => response.json())
            .subscribe(
              response => {
                this.loading = false;
                Materialize.toast(`Lesson added successfully`, 5000);
                this.router.navigate(['../../topics', this.topicId], {relativeTo: this.route});
              },
              error => {
                console.warn("lesson was created but unable to create lsections");
                this.loading = false;
                Materialize.toast(`Oops! Something bad happened, retry`, 5000);
              })
        },
        error => {
          this.loading = false;
          Materialize.toast(`Oops! Something bad happened, retry`, 5000);
        });
  }

  addNewSection() {
    if (!this.lsections)
      this.lsections = [];
    this.lsections.push({
      "mediaURI": "",
      "imageURI": "",
      "section_description": "",
      "archived": false
    })
  }

  lessonIconUpload(event) {
    this.loading = true;
    let file = event.srcElement.files[0];
    if (file && this.common.checkSupportedFormat(file, 'image')) {
      this.storageRef.child(`${'lessons/icons'}/${new Date().getTime() + file.name}`).put(file)
        .then((snapshot) => {
          this.loading = false;
          this.lesson_icon = snapshot.downloadURL;
          Materialize.toast(`Lesson Icon successfully uploaded`, 5000);
        })
        .catch(error => {
          this.loading = false;
          Materialize.toast(`Oops! Something bad happened, retry`, 5000);
        })
    }
    else {
      Materialize.toast(`Unsupported file or not an image`, 5000);
      this.loading = false;
    }
  }

  uploadMedia(event, i) {
    console.log("file ", event.srcElement.files[0]);
    this.loading = true;
    this.videoUpdate = false;
    let file = event.srcElement.files[0];
    if (file && this.common.checkSupportedFormat(file, 'video')) {
      this.storageRef.child(`${'lessons/videos'}/${new Date().getTime() + file.name}`).put(file)
        .then(snapshot => {
          this.loading = false;
          this.videoUpdate = true;
          this.lsections[i].mediaURI = snapshot.downloadURL;
          Materialize.toast(`Media successfully uploaded`, 5000);
        })
        .catch(error => {
          console.log("in catch", error);
          this.loading = false;
          this.videoUpdate = true;
          Materialize.toast(`Oops! Something bad happened, retry`, 5000);
        })
    }
    else {
      Materialize.toast(`Unsupported file or not a video`, 5000);
      this.loading = false;
    }
  }

  attachImage(event, i) {
    this.loading = true;
    let file = event.srcElement.files[0];
    if (file && this.common.checkSupportedFormat(file, 'image')) {
      this.storageRef.child(`${'lessons/pictures'}/${new Date().getTime() + file.name}`).put(file)
        .then(snapshot => {
          this.loading = false;
          this.lsections[i].imageURI = snapshot.downloadURL;
          Materialize.toast(`Image successfully uploaded`, 5000);
        })
        .catch(error => {
          this.loading = false;
          Materialize.toast(`Oops! Something bad happened, retry`, 5000);
        })
    }
    else {
      Materialize.toast(`Unsupported file or not a video`, 5000);
      this.loading = false;
    }
  }
}
